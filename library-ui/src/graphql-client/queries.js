import { gql } from '@apollo/client';

const getBooks = gql`
    query getBookQuery($page: Int, $limit: Int) {
        books(page: $page, limit: $limit) {
            id
            name
            genre {
                id
                name
            }
            author {
                id
                name
                age
            }
            thumbUrl
            pages
            description
            createdAt
            updatedAt
        }
        pageInfo(page: $page, limit: $limit) {
            totalPages
        }
    }
`;

const getSingleBook = gql`
    query getSingleBookQuery($id: ID!) {
        book(id: $id) {
            id
            name
            genre {
                id
                name
            }
            author {
                id
                name
                age
                books {
                    id
                    name
                }
            }
            thumbUrl
            pages
            description
            createdAt
            updatedAt
        }
    }
`;

const getAuthors = gql`
    query getAnAuthor {
        authors {
            id
            name
        }
    }
`;

const getGenres = gql`
    query getAllGenres {
        genres {
            id
            name
        }
    }
`;

export { getBooks, getSingleBook, getAuthors, getGenres };
