import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import { useEffect, useState } from 'react';
import BookDetails from '~/components/BookDetails';
import ListBooks from '~/components/ListBooks';
import { getBooks, getGenres, getSingleBook } from '~/graphql-client/queries';

function Home() {
    const [classifyData, setClassifyData] = useState([]);
    const [bookId, setBookId] = useState(null);
    const { data: BooksData } = useQuery(getBooks);
    const { data: genreData } = useQuery(getGenres);
    const { data: singleBookData, refetch } = useQuery(getSingleBook, {
        variables: {
            id: bookId ?? BooksData?.books[0].id,
        },
    });

    useEffect(() => {
        const rawData = genreData?.genres?.map((genre) => ({
            [genre.name]: BooksData?.books?.filter((book) => book?.genre?.name === genre.name),
        }));
        setClassifyData(rawData);
    }, [genreData, BooksData]);

    useEffect(() => {
        refetch();
    }, [bookId]);

    const handleSetBookId = (e, id) => {
        e.stopPropagation();
        setBookId(id);
    };

    return (
        <Box display="flex">
            <Box width="72%" style={{ backgroundColor: '#fff' }}>
                <ListBooks title="Trending Books" data={BooksData?.books} onClickItem={handleSetBookId} />
                {genreData?.genres?.map((genre, index) => (
                    <ListBooks
                        key={genre?.id}
                        title={genre?.name}
                        data={classifyData?.length > 0 ? Object.values(classifyData[index]) : []}
                        onClickItem={handleSetBookId}
                    />
                ))}
            </Box>
            <Box
                width="25%"
                height="88vh"
                ml={1}
                style={{ backgroundColor: '#fff', overscrollBehaviorY: 'contain' }}
                position="fixed"
                right="0"
                top="100px"
                overflow="scroll"
            >
                <BookDetails
                    title={singleBookData?.book?.name}
                    author={singleBookData?.book?.author?.name}
                    thumbUrl={singleBookData?.book?.thumbUrl}
                    date={singleBookData?.book?.updatedAt?.split(' ')[0]}
                    pages={singleBookData?.book?.pages}
                    description={singleBookData?.book?.description}
                    buttonName="Chapter List"
                    buttonStyle="contained"
                    color="primary"
                />
            </Box>
        </Box>
    );
}

export default Home;
