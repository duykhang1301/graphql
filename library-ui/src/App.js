/* eslint-disable react/react-in-jsx-scope */
import { Fragment } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { publicRoutes } from './routes';
import { ColorModeContext, useMode } from './theme';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
// import ContextProvider from "./context/ContextProvider";
import DefaultLayout from './layouts/DefaultLayout';

const client = new ApolloClient({
    uri: 'http://localhost:4000/graphql',
    cache: new InMemoryCache({
        addTypename: false,
    }),
});

function App() {
    const [theme, colorMode] = useMode();
    return (
        <Router>
            {/* <ContextProvider> */}
            <ApolloProvider client={client}>
                <ColorModeContext.Provider value={colorMode}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline />
                        <div className="App">
                            <Routes>
                                {publicRoutes.map((route, index) => {
                                    const Page = route.component;
                                    let Layout = DefaultLayout;

                                    if (route.layout) {
                                        Layout = route.layout;
                                    } else if (route.layout === null) {
                                        Layout = Fragment;
                                    }
                                    return (
                                        <Route
                                            key={index}
                                            path={route.path}
                                            element={
                                                <Layout>
                                                    <Page />
                                                </Layout>
                                            }
                                        />
                                    );
                                })}
                            </Routes>
                        </div>
                    </ThemeProvider>
                </ColorModeContext.Provider>
            </ApolloProvider>
            {/* </ContextProvider> */}
        </Router>
    );
}

export default App;
