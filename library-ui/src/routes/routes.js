//Layouts
import DefaultLayout from "~/layouts/DefaultLayout";

import config from "~/config";
//Pages

import Home from "../pages/Home";

const publicRoutes = [
  { path: config.routes.home, component: Home, layout: DefaultLayout },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };
