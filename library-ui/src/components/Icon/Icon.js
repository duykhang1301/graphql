import { IconButton } from "@material-ui/core";

function Icon({ icon, type, customStyles, onClick = () => {} }) {
  return (
    <IconButton type={type} style={customStyles} onClick={onClick}>
      {icon}
    </IconButton>
  );
}

export default Icon;
