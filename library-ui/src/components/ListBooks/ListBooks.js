import { Box, IconButton, Typography } from '@material-ui/core';
import { ChevronLeft, ChevronRight, CreateOutlined, DateRangeOutlined, FileCopyOutlined } from '@material-ui/icons';
import { useEffect, useState } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

const TOTAL_ITEMS_0 = 1;
const TOTAL_ITEMS_568 = 2;
const TOTAL_ITEMS_1024 = 4;
const responsive = {
    468: { items: TOTAL_ITEMS_0 },
    568: { items: TOTAL_ITEMS_568 },
    1024: { items: TOTAL_ITEMS_1024 },
};

function ListBooks({ title, data, onClickItem }) {
    const [dataLength, setDataLength] = useState(data?.length);
    const [booksData, setBooksData] = useState([]);
    const [activeIndex, setActiveIndex] = useState(0);
    const slidePrev = () => setActiveIndex(() => (activeIndex <= 0 ? 0 : activeIndex - 1));
    const slideNext = () =>
        setActiveIndex(() => (activeIndex >= dataLength - TOTAL_ITEMS_1024 ? activeIndex : activeIndex + 1));
    const syncActiveIndex = ({ item }) => setActiveIndex(item);

    useEffect(() => {
        if (data) {
            let dataAfterCheck = [];
            const isDataChildArray = Array.isArray(data[0]);
            if (isDataChildArray) {
                dataAfterCheck = data[0];
            } else {
                dataAfterCheck = data;
            }
            setDataLength(dataAfterCheck.length);
            const rawData = dataAfterCheck?.map((book) => {
                return (
                    <Box
                        key={book?.id}
                        width="200px"
                        borderRadius="8px"
                        padding="10px"
                        ml={2}
                        onMouseUp={(e) => onClickItem(e, book?.id)}
                        style={{
                            maxHeight: '420px',
                            userSelect: 'none',
                            cursor: 'pointer',
                            border: '1px solid rgba(26, 25, 25, 0.07)',
                        }}
                    >
                        <img
                            src={book?.thumbUrl}
                            alt=""
                            width="100%"
                            height="280px"
                            style={{ pointerEvents: 'none', borderRadius: '8px' }}
                        />
                        <h5
                            width="100%"
                            height="15px"
                            style={{ marginTop: '8px', fontSize: '16px', textOverflow: 'ellipsis', overflow: 'hidden' }}
                        >
                            {book?.name}
                        </h5>
                        <Typography variant="h6" width="100%" style={{ marginTop: '8px', fontSize: '12px !important' }}>
                            <CreateOutlined style={{ fontSize: '14px' }} /> {book?.author?.name}
                        </Typography>
                        <Typography variant="h6" width="100%" style={{ marginTop: '8px', fontSize: '12px !important' }}>
                            <DateRangeOutlined style={{ fontSize: '14px' }} /> {book?.updatedAt?.split(' ')[0]}
                        </Typography>
                        <Typography variant="h6" width="100%" style={{ marginTop: '8px' }}>
                            <FileCopyOutlined style={{ fontSize: '14px' }} /> {book?.pages} pages
                        </Typography>
                    </Box>
                );
            });
            setBooksData(rawData);
        }
    }, [data]);

    console.log(data);
    return (
        <Box
            p={3}
            sx={{
                '& .alice-carousel__stage-item': {
                    whiteSpace: 'nowrap !important',
                    textOverflow: 'hidden !important',
                },
            }}
        >
            <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                mb={3}
                sx={{
                    '& .MuiButtonBase-root': {
                        border: '1px solid rgba(26, 25, 25, 0.07)',
                        marginLeft: '8px',
                        padding: '8px',
                    },
                }}
            >
                <Typography variant="h3">{title}</Typography>
                <Box>
                    <IconButton onClick={slidePrev}>
                        <ChevronLeft />
                    </IconButton>
                    <IconButton onClick={slideNext}>
                        <ChevronRight />
                    </IconButton>
                </Box>
            </Box>
            <AliceCarousel
                mouseTracking
                items={booksData}
                responsive={responsive}
                controlsStrategy="alternate"
                disableDotsControls
                disableButtonsControls
                activeIndex={activeIndex}
                onSlideChanged={syncActiveIndex}
                touchTracking={true}
            />
        </Box>
    );
}

export default ListBooks;
