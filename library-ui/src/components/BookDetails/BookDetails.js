import { Box, Button, Typography } from '@material-ui/core';
import { CreateOutlined, DateRangeOutlined, FileCopyOutlined } from '@material-ui/icons';

function BookDetails({ thumbUrl, title, author, date, pages, description, buttonName, buttonStyle, color }) {
    return (
        <>
            <Box display="flex" justifyContent="flex-start" mt={2}>
                <Box>
                    <img
                        src={thumbUrl}
                        width="120px"
                        height="100%"
                        style={{ cursor: 'pointer', borderRadius: '8px', objectFit: 'cover' }}
                    />
                </Box>
                <Box ml={1}>
                    <Typography variant="h3" style={{ marginBottom: '12px' }}>
                        {title}
                    </Typography>
                    <Typography variant="h5" style={{ marginBottom: '12px', fontWeight: 600, fontSize: '14px' }}>
                        <CreateOutlined style={{ fontSize: '14px', marginRight: '4px' }} />
                        {author}
                    </Typography>
                    <Typography variant="h5" style={{ marginBottom: '12px', fontWeight: 600, fontSize: '14px' }}>
                        <DateRangeOutlined style={{ fontSize: '14px', marginRight: '4px' }} />
                        {date}
                    </Typography>
                    <Typography variant="h5" style={{ marginBottom: '12px', fontWeight: 600, fontSize: '14px' }}>
                        <FileCopyOutlined style={{ fontSize: '14px', marginRight: '4px' }} />
                        {pages} pages
                    </Typography>
                    <Box
                        width="100% !important"
                        sx={{
                            '& .MuiButton-containedPrimary': {
                                marginTop: '5px',
                                background: '#6870fa',
                            },
                            '& .MuiButton-label': {
                                textTransform: 'none',
                                fontSize: '14px',
                                fontWeight: '600',
                            },
                        }}
                    >
                        <Button variant={buttonStyle} color={color} type="button">
                            {buttonName}
                        </Button>
                    </Box>
                </Box>
            </Box>
            <Box width="95%" mt={2}>
                <Typography variant="h6" style={{ textAlign: 'justify' }}>
                    {description}
                </Typography>
            </Box>
            <Box
                width="100% !important"
                display="flex"
                alignItems="center"
                justifyContent="center"
                sx={{
                    '& .MuiButtonBase-root': {
                        padding: '5px',
                        marginTop: '10px',
                        marginBottom: '20px',
                        width: '90%',
                        borderRadius: '20px',
                    },
                    '& .MuiButton-containedPrimary': {
                        background: '#6870fa',
                    },
                    '& .MuiButton-label': {
                        textTransform: 'none',
                        fontSize: '14px',
                        fontWeight: '600',
                    },
                }}
            >
                <Button variant={buttonStyle} color={color} type="button">
                    Reading Now
                </Button>
            </Box>
        </>
    );
}

export default BookDetails;
