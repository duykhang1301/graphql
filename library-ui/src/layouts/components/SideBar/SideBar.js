/* eslint-disable no-unused-vars */
import { Box, useTheme, Typography, IconButton } from '@material-ui/core';
import { Sidebar, Menu, MenuItem } from 'react-pro-sidebar';
import { useState } from 'react';
import { tokens } from '~/theme';
import { Link } from 'react-router-dom';
import {
    AccountCircle,
    AssistantOutlined,
    ChevronLeft,
    ChevronRight,
    DnsOutlined,
    HomeOutlined,
    LibraryBooksOutlined,
    LocalLibraryOutlined,
    MenuBookOutlined,
} from '@material-ui/icons';

const Item = ({ title, to, icon, selected, setSelected }) => {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    return (
        <MenuItem
            active={selected === title}
            style={{
                color: colors.grey[100],
            }}
            onClick={() => setSelected(title)}
            icon={icon}
            component={<Link to={to}></Link>}
        >
            <Typography variant="h6">{title}</Typography>
        </MenuItem>
    );
};

function SideBar({ isCollapsed, setIsCollapsed }) {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const [selected, setSelected] = useState('Book Store');
    return (
        <Box
            pt={3}
            sx={{
                '& .ps-sidebar-container': {
                    background: `${colors.primary[400]} !important`,
                },
                '& .pro-icon-wrapper': {
                    backgroundColor: 'transparent !important',
                },
                '& .pro-inner-item': {
                    padding: '5px 35px 5px 20px !important',
                },
                '& .pro-inner-item:hover': {
                    color: '#868dfb !important',
                },
                '& .ps-active': {
                    color: '#6870fa !important',
                },
                zIndex: '9999',
            }}
        >
            <Sidebar
                collapsed={isCollapsed}
                style={{
                    boxShadow: '0px 1px 1px rgb(0 0 0 / 12%)',
                    zIndex: '999',
                    position: 'fixed',
                    top: '0',
                    transition: 'all 0.4s cubic-bezier(0.65, 0, 0.35, 1) !important',
                    backgroundColor: `${colors.primary[400]} !important`,
                }}
                breakPoint="md"
            >
                <Menu
                    style={{
                        height: '100vh',
                        overflowY: 'scroll',
                    }}
                >
                    {/* <MenuItem
                        onClick={() => setIsCollapsed(!isCollapsed)}
                        icon={isCollapsed ? <MenuOutlined /> : undefined}
                    > */}
                    {!isCollapsed ? (
                        <Box display="flex" justifyContent="flex-start" alignItems="center" ml="15px" mt={3}>
                            <Box display="flex" mr={5}>
                                <LibraryBooksOutlined
                                    style={{
                                        color: colors.grey[100],
                                        fontSize: '25px',
                                        marginRight: '5px',
                                    }}
                                />
                                <Typography
                                    variant="h3"
                                    style={{
                                        color: colors.grey[100],
                                    }}
                                >
                                    Bukstore
                                </Typography>
                            </Box>
                            <Box
                                sx={{
                                    '& .MuiButtonBase-root': {
                                        border: '1px solid rgba(26, 25, 25, 0.07)',
                                        marginLeft: '8px',
                                        padding: '8px',
                                    },
                                }}
                            >
                                <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                                    <ChevronLeft />
                                </IconButton>
                            </Box>
                        </Box>
                    ) : (
                        <Box
                            display="flex"
                            justifyContent="space-between"
                            alignItems="center"
                            ml="15px"
                            mt={3}
                            sx={{
                                '& .MuiButtonBase-root': {
                                    border: '1px solid rgba(26, 25, 25, 0.07)',
                                    padding: '8px',
                                },
                            }}
                        >
                            <IconButton onClick={() => setIsCollapsed(!isCollapsed)}>
                                <ChevronRight />
                            </IconButton>
                        </Box>
                    )}
                    {/* </MenuItem> */}

                    <Box paddingTop="20px">
                        {!isCollapsed && (
                            <Typography
                                variant="h5"
                                style={{
                                    margin: '20px 0 10px 20px',
                                    color: colors.grey[100],
                                }}
                            >
                                MENUS
                            </Typography>
                        )}
                        <Item
                            title="Reading Now"
                            to="/admin"
                            selected={selected}
                            setSelected={setSelected}
                            icon={<MenuBookOutlined />}
                        />
                        <Item
                            title="Book Store"
                            to="/admin"
                            selected={selected}
                            setSelected={setSelected}
                            icon={<HomeOutlined />}
                        />
                        <Item
                            title="My Book"
                            to="/admin"
                            selected={selected}
                            setSelected={setSelected}
                            icon={<LocalLibraryOutlined />}
                        />
                        <Item
                            title="Wishlist"
                            to="/admin"
                            selected={selected}
                            setSelected={setSelected}
                            icon={<DnsOutlined />}
                        />
                        <Item
                            title="Subscription"
                            to="/admin"
                            selected={selected}
                            setSelected={setSelected}
                            icon={<AssistantOutlined />}
                        />
                        <Item
                            title="My Account"
                            to="/admin"
                            selected={selected}
                            setSelected={setSelected}
                            icon={<AccountCircle />}
                        />
                    </Box>
                </Menu>
            </Sidebar>
        </Box>
    );
}

export default SideBar;
