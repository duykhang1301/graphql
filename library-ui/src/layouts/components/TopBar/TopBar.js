import { Box, IconButton, InputBase, Typography, useTheme } from '@material-ui/core';
import { useContext, useMemo } from 'react';
import { ColorModeContext, tokens } from '~/theme';
import Icon from '~/components/Icon';
import {
    Brightness2Outlined,
    WbSunnyOutlined,
    SearchOutlined,
    NotificationsOutlined,
    KeyboardArrowDown,
} from '@material-ui/icons';

function TopBar({ isSidebarCollapsed }) {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const colorMode = useContext(ColorModeContext);

    const ACTION_ICONS = useMemo(
        () => [
            {
                icon: theme.palette.mode === 'dark' ? <WbSunnyOutlined /> : <Brightness2Outlined />,
                action: colorMode.toggleColorMode,
            },
            {
                icon: <NotificationsOutlined />,
            },
        ],
        [theme.palette.mode],
    );

    const customStylesIcon = useMemo(
        () => ({
            p: 1,
            color: colors.grey[400],
        }),
        [],
    );

    return (
        <Box
            display="flex"
            justifyContent="space-between"
            p={3}
            position="fixed"
            style={{
                width: '100%',
                maxWidth: '100%',
                boxShadow: '0px 1px 1px rgb(0 0 0 / 12%)',
                zIndex: '999',
                flex: '1',
                paddingLeft: `${isSidebarCollapsed ? 100 : 269}px`,
                backgroundColor: '#fff',
            }}
        >
            {/* Search Bar */}
            <Box
                display="flex"
                justifyContent="space-between"
                width={!isSidebarCollapsed ? '90%' : '88%'}
                borderRight="1px solid rgba(26, 25, 25, 0.07)"
                pr={2}
            >
                <Typography
                    variant="h2"
                    style={{
                        marginRight: '20%',
                        fontWeight: 600,
                    }}
                >
                    My Books
                </Typography>
                <Box display="flex">
                    <Box
                        display="flex"
                        borderRadius="8px"
                        width="20rem"
                        pl={2}
                        sx={{ backgroundColor: colors.primary[400] }}
                        style={{
                            border: `1px solid rgba(26, 25, 25, 0.07)`,
                            marginRight: '20px',
                        }}
                    >
                        <InputBase style={{ ml: 2, flex: 1, color: colors.grey[400] }} placeholder="Search...." />
                        <Icon type="button" customStyles={customStylesIcon} icon={<SearchOutlined />} />
                    </Box>
                    {/* Action Icons */}
                    <Box>
                        {ACTION_ICONS.map((actionItem, index) => (
                            <Icon
                                key={index}
                                type="button"
                                customStyles={customStylesIcon}
                                icon={actionItem.icon}
                                onClick={actionItem?.action !== undefined ? actionItem.action : () => {}}
                            />
                        ))}
                    </Box>
                </Box>
            </Box>
            <Box
                width="30%"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginLeft: '20px',
                }}
            >
                <Box
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                    }}
                >
                    <img
                        src="https://images2.thanhnien.vn/528068263637045248/2023/4/4/photo-1680584585445-1680584586463367033622.jpeg"
                        width="40px"
                        height="40px"
                        style={{ cursor: 'pointer', borderRadius: '50%', objectFit: 'cover' }}
                    />
                    <Box ml={1}>
                        <Typography variant="h5">Duy Khang</Typography>
                        <Typography variant="h6">Trial 17 days</Typography>
                    </Box>
                </Box>
                <Box
                    sx={{
                        '& .MuiButtonBase-root': {
                            border: '1px solid rgba(26, 25, 25, 0.07)',
                            marginLeft: '8px',
                            padding: '8px',
                        },
                    }}
                >
                    <IconButton>
                        <KeyboardArrowDown />
                    </IconButton>
                </Box>
            </Box>
        </Box>
    );
}

export default TopBar;
