import TopBar from '~/layouts/components/TopBar';
import SideBar from '../components/SideBar/SideBar';
import { Box } from '@material-ui/core';
import { useState } from 'react';

function DefaultLayout({ children }) {
    const [isCollapsed, setIsCollapsed] = useState(true);
    return (
        <Box>
            <Box display="flex" maxWidth="100vw">
                <SideBar isCollapsed={isCollapsed} setIsCollapsed={setIsCollapsed} />
                <Box flex="1" maxWidth="100%">
                    <TopBar isSidebarCollapsed={isCollapsed} />
                    <Box style={{ marginTop: '95px', paddingLeft: `${isCollapsed ? 80 : 249}px` }}>{children}</Box>
                </Box>
            </Box>
        </Box>
    );
}

export default DefaultLayout;
