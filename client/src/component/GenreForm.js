import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useMutation } from "@apollo/client";
import { useState } from "react";
import { addGenre } from "../graphql-client/mutation";
import { getGenres } from "../graphql-client/queries";

function GenreForm() {
  const [addNameGenre, dataMutation] = useMutation(addGenre);
  const [newGenre, setNewGenre] = useState({
    name: "",
  });

  const { name } = newGenre;

  const onInputChange = (event) => {
    setNewGenre((prevGenre) => ({
      ...prevGenre,
      [event.target.name]: event.target.value,
    }));
  };

  const onSubmit = (event) => {
    event.preventDefault();

    addNameGenre({
      variables: {
        name,
      },
      refetchQueries: [{ query: getGenres }],
    });

    setNewGenre({
      name: "",
    });
  };

  return (
    <Form onSubmit={onSubmit}>
      <Form.Group className="invisible">
        <Form.Control
          type="text"
          placeholder="Genre"
          name="name"

          //onChange={onIuputChange}
          //value={name}
        />
      </Form.Group>
      <Form.Group className="invisible">
        <Form.Control
          type="text"
          placeholder="Genre"
          name="name"

          //onChange={onIuputChange}
          //value={name}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Genre"
          name="name"
          onChange={onInputChange}
          value={name}
        />
      </Form.Group>
      <Button className="float-right" variant="info" type="submit">
        Add Genre
      </Button>
    </Form>
  );
}

export default GenreForm;
