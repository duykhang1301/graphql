import { useState } from "react";
import Card from "react-bootstrap/Card";
import CardColumns from "react-bootstrap/CardColumns";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import BookDetails from "./BookDetails";

import { useQuery } from "@apollo/client";
import { getBooks } from "../graphql-client/queries";
function BookList() {
  const [bookId, setBookId] = useState(null);
  const { loading, error, data } = useQuery(getBooks, {
    variables: null,
  });
  console.log(data);
  if (loading) return <p>Loading Books....</p>;
  if (error) return <p>Error Loading Books</p>;
  return (
    <Row>
      <Col xs={8}>
        <CardColumns>
          {data.books.map((book) => (
            <Card
              border="info"
              text="info"
              className="text-center shadow "
              style={{ cursor: "pointer" }}
              key={book.id}
              onClick={() => setBookId(book.id)}
            >
              <Card.Body>{book.name}</Card.Body>
            </Card>
          ))}
        </CardColumns>
      </Col>
      <Col>
        <BookDetails bookId={bookId} />
      </Col>
    </Row>
  );
}

export default BookList;
