/* eslint-disable react-hooks/exhaustive-deps */
import Container from "react-bootstrap/esm/Container";
import Spinner from "react-bootstrap/Spinner";
import Pagination from "react-bootstrap-4-pagination";
import Table from "./Table";
import { useQuery } from "@apollo/client";
import { getAuthors, getBooks, getGenres } from "../graphql-client/queries";
import { useEffect, useState } from "react";
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import Form from "react-bootstrap/esm/Form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";

function DetailsBookPage() {
  const [page, setPage] = useState(1);
  const [dataStorage, setDataStorage] = useState([]);
  const [updatePage, setUpdatePage] = useState(false);
  const [bookId, setBookId] = useState(null);
  const [loadingUpdatePage, setLoadingUpdatePage] = useState(false);
  const [dataFilter, setDataFilter] = useState({
    genre: "ALL",
    author: "ALL",
  });
  const { loading, data, refetch } = useQuery(getBooks, {
    variables: {
      page: page,
      limit: 10,
    },
    fetchPolicy: "network-only",
  });

  const { data: authorsData } = useQuery(getAuthors);
  const { data: genresData } = useQuery(getGenres);

  useEffect(() => {
    if (updatePage) {
      setDataStorage((prevData) => {
        delete prevData[bookId].updateInfo;
        return [prevData[bookId]];
      });
    } else {
      let newData = [];
      const { genre, author } = dataFilter;
      if (genre === "ALL" && author === "ALL") {
        newData = handleConvertData(data?.books);
      } else {
        const prevData = handleConvertData(data?.books);
        const dataFilter = prevData?.filter((data) => {
          if (genre === "ALL" || author === "ALL") {
            return data.genre.name === genre || data.author.name === author;
          } else {
            return data.genre.name === genre && data.author.name === author;
          }
        });
        newData = handleConvertData(dataFilter);
      }
      setDataStorage(newData);
    }
    setTimeout(() => {
      setLoadingUpdatePage(false);
    }, 250);
  }, [bookId, updatePage, data, dataFilter]);

  useEffect(() => {
    refetch();
  }, [page]);

  const handleConvertData = (booksData) => {
    const dataConvert = booksData?.map((book, index) => {
      return (book = { ...book, updateInfo: `Update ${index}` });
    });
    return dataConvert;
  };

  const handleUpdateData = (id) => {
    setBookId(+id);
    setUpdatePage(true);
    setLoadingUpdatePage(true);
  };

  const handleBack = () => {
    setUpdatePage(false);
    setLoadingUpdatePage(true);
  };

  const handleFilterBooks = (event) => {
    setDataFilter((prevDataFilter) => ({
      ...prevDataFilter,
      [event.target.name]: event.target.value,
    }));
  };

  const mdSize = {
    totalPages: data?.pageInfo.totalPages,
    currentPage: page,
    showMax: 5,
    threeDots: true,
    prevNext: true,
    onClick: (page) => {
      setPage(page);
    },
  };

  return (
    <>
      {loading || loadingUpdatePage ? (
        <Row className="text-center mt-5">
          <Col className="text-center mt-5">
            <Spinner animation="border" />
          </Col>
        </Row>
      ) : (
        <Container className="m-0">
          {updatePage ? (
            <Table
              data={dataStorage}
              backButton
              onUpdateData={handleBack}
              refetchDataBook={refetch}
              updateFields
              listDataSelect={{
                genre: genresData.genres,
                authorId: authorsData.authors,
              }}
              allowFieldSelect={["name"]}
              listKeySelect={["genre", "authorId"]}
              title="Update Book"
            />
          ) : (
            <>
              <Form>
                <Row>
                  <Col className="p-0 mt-4">
                    <h3> Filter {<FontAwesomeIcon icon={faFilter} />}</h3>
                  </Col>
                </Row>
                <Row className="mt-2 p-0">
                  <Col md={3} className="p-0">
                    <Form.Group>
                      <Form.Control
                        as="select"
                        name="genre"
                        onChange={handleFilterBooks}
                        value={dataFilter.genre}
                      >
                        <option value="ALL" selected>
                          All Genres
                        </option>
                        {genresData.genres.map((genre) => (
                          <option key={genre.id} value={genre.name}>
                            {genre.name}
                          </option>
                        ))}
                      </Form.Control>
                    </Form.Group>
                  </Col>
                  <Col md={3} className="pl-2">
                    <Form.Group>
                      <Form.Control
                        as="select"
                        name="author"
                        onChange={handleFilterBooks}
                        value={dataFilter.author}
                      >
                        <option value="ALL" selected>
                          All Authors
                        </option>
                        {authorsData.authors.map((author) => (
                          <option key={author.id} value={author.name}>
                            {author.name}
                          </option>
                        ))}
                      </Form.Control>
                    </Form.Group>
                  </Col>
                </Row>
              </Form>
              <Table
                data={dataStorage}
                onUpdateData={handleUpdateData}
                title="Book Management"
              />
              <Row>
                <Col md={12} className="mt-5 text-center">
                  {dataStorage && <Pagination {...mdSize} circle shadow />}
                </Col>
              </Row>
            </>
          )}
        </Container>
      )}
    </>
  );
}

export default DetailsBookPage;
