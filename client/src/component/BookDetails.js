import Card from "react-bootstrap/Card";
import { useQuery } from "@apollo/client";
import { getSingleBook } from "../graphql-client/queries";
function BookDetails({ bookId }) {
  const { loading, error, data } = useQuery(getSingleBook, {
    variables: {
      id: bookId,
    },
    skip: bookId === null,
  });
  if (loading) {
    return (
      <p
        style={{
          height: "350px",
          backgroundColor: "#17a2b8",
          borderRadius: "4px",
          color: "#fff",
        }}
      >
        Loading book details
      </p>
    );
  }
  if (error) {
    console.log(error.message);
    return <p style={{ height: "350px" }}>Error loading book details</p>;
  }

  const book = bookId !== null && data !== undefined ? data.book : null;
  return (
    <Card bg="info" text="white" className="shadow" style={{ height: "auto" }}>
      {book === null ? (
        <Card.Text>Please select a book</Card.Text>
      ) : (
        <>
          <Card.Body>
            <Card.Title>{book.name}</Card.Title>
            <Card.Subtitle>{book.genre.name}</Card.Subtitle>
            <p>{book.author.name}</p>
            <p>{book.author.age}</p>
            <p>All books by this author</p>

            <ul>
              {book.author.books.map((book) => (
                <li key={book.id}>{book.name}</li>
              ))}
            </ul>
          </Card.Body>
        </>
      )}
    </Card>
  );
}

export default BookDetails;
