import { useState } from "react";
import { getAuthors, getBooks, getGenres } from "../graphql-client/queries";
import { addSingleBook } from "../graphql-client/mutation";

import { useQuery, useMutation } from "@apollo/client";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
function BookForm() {
  const { loading, data } = useQuery(getAuthors);
  const { data: genresData } = useQuery(getGenres);
  const [newBook, setNewBook] = useState({
    name: "",
    genre: "",
    authorId: "",
  });
  const { name, genre, authorId } = newBook;
  const onInputChange = (event) => {
    setNewBook((prevBook) => ({
      ...prevBook,
      [event.target.name]: event.target.value,
    }));
  };

  const onSubmit = (event) => {
    event.preventDefault();

    addBook({
      variables: {
        name,
        genre,
        authorId,
      },
      refetchQueries: [{ query: getBooks }],
    });

    setNewBook({
      name: "",
      genre: "",
      authorId: "",
    });
  };

  const [addBook, dataMutation] = useMutation(addSingleBook);

  return (
    <Form onSubmit={onSubmit}>
      <Form.Group>
        <Form.Control
          type="text"
          placeholder="Book name"
          name="name"
          onChange={onInputChange}
          value={name}
        />
      </Form.Group>
      <Form.Group>
        <Form.Control
          as="select"
          name="genre"
          onChange={onInputChange}
          value={genre}
        >
          <option value="" disabled>
            Select genre
          </option>
          {genresData?.genres.map((genre) => (
            <option key={genre.id} value={genre.id}>
              {genre.name}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
      <Form.Group>
        {loading ? (
          <p>Loading author....</p>
        ) : (
          <Form.Control
            as="select"
            name="authorId"
            onChange={onInputChange}
            value={authorId}
          >
            <option value="" disabled>
              Select author
            </option>
            {data.authors.map((author) => (
              <option key={author.id} value={author.id}>
                {author.name}
              </option>
            ))}
          </Form.Control>
        )}
      </Form.Group>
      <Button className="float-right" variant="info" type="submit">
        Add Book
      </Button>
    </Form>
  );
}

export default BookForm;
