import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import AuthorForm from "./AuthorForm";
import BookForm from "./BookForm";
import GenreForm from "./GenreForm";

function Forms() {
  return (
    <Row>
      <Col>
        <BookForm />
      </Col>
      <Col>
        <AuthorForm />
      </Col>
      <Col>
        <GenreForm />
      </Col>
    </Row>
  );
}

export default Forms;
