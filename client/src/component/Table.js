/* eslint-disable array-callback-return */
import { useEffect, useRef, useState } from "react";
import Button from "react-bootstrap/esm/Button";
import Col from "react-bootstrap/esm/Col";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import Row from "react-bootstrap/esm/Row";
import { useMutation } from "@apollo/client";
import { updateSingleBook } from "../graphql-client/mutation";

const DEFAULT_WIDTH = 12;

function Table({
  data,
  onUpdateData = () => {},
  refetchDataBook = () => {},
  backButton = false,
  updateFields = false,
  allowFieldSelect = [],
  listDataSelect,
  listKeySelect,
  title,
}) {
  const [dataTable, setDataTable] = useState([]);
  const [fields, setFields] = useState([]);
  const [widthColumns, setWidthColums] = useState(0);
  const [dataUpdateBook, setDataUpdateBook] = useState({
    id: "",
    name: "",
    genre: "",
    authorId: "",
  });

  const [updateBook, dataMutation] = useMutation(updateSingleBook);

  const backButtonRef = useRef();

  useEffect(() => {
    const rawData = [];
    data?.length === 0 && handleResetDataTable();
    if (Array.isArray(data)) {
      data.map((data, index) => {
        const splitData = Object.entries(data);
        index < 1 && handleGetField(data);
        handleInputData(splitData, rawData);
      });

      setDataUpdateBook({
        id: data[0]?.id,
        name: data[0]?.name,
        genre: data[0]?.genre.id,
        authorId: data[0]?.author.id,
      });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, backButton]);

  const handleResetDataTable = () => {
    setFields([]);
    setDataTable([]);
  };

  const handleGetField = (listData) => {
    const rawField = Object.keys(listData);
    setFields(rawField);
    setWidthColums(Math.floor(DEFAULT_WIDTH / rawField.length));
  };

  const handleInputData = (listData, rawData) => {
    const fieldHasListed = [];
    listData.map((data) => {
      const [keyListSelect, value] = data;
      if (typeof value === "object") {
        fieldHasListed.push(keyListSelect);
        const valueAuthor = Object.entries(value).map((item, indexTotal) => {
          const [keyField, value] = item;
          return (item = keyField !== "id" && (
            <Row
              className="p-0"
              key={indexTotal}
              style={{
                height: "100%",
              }}
            >
              <Col
                md={3}
                className="text-center border-right  border-bottom p-0"
                style={{
                  height: "100%",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {keyField}
              </Col>
              <Col
                md={9}
                className="text-center border-right border-bottom p-0"
                style={{
                  height: "100%",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {updateFields && allowFieldSelect.includes(keyField) ? (
                  listKeySelect.map((primaryKeyList) => {
                    return Object.entries(listDataSelect).map(
                      (dataItem, index) => {
                        const [keyData, valueData] = dataItem;
                        const compareResult = primaryKeyList === keyData;
                        return (
                          compareResult &&
                          valueData.some((item) => item.name === value) && (
                            <select
                              key={index}
                              name={listKeySelect[index]}
                              onChange={handleDataChange}
                            >
                              <option selected value={primaryKeyList}>
                                {value}
                              </option>
                              {valueData.map(
                                (data) =>
                                  data.name !== value && (
                                    <option key={data.id} value={data.id}>
                                      {data.name}
                                    </option>
                                  )
                              )}
                            </select>
                          )
                        );
                      }
                    );
                  })
                ) : updateFields ? (
                  <input
                    type="text"
                    name={keyField}
                    value={value}
                    onChange={handleDataChange}
                    autoComplete="off"
                    className="text-center"
                    style={{ width: "100%" }}
                  />
                ) : (
                  value
                )}
              </Col>
            </Row>
          ));
        });
        rawData.push(valueAuthor);
      } else {
        rawData.push(value);
      }
    });

    setDataTable(rawData);
  };

  const handleUpdateInfoBook = () => {
    const { id, name, genre, authorId } = dataUpdateBook;
    updateBook({
      variables: {
        id,
        name,
        genre,
        authorId,
      },
      onCompleted: () => refetchDataBook(),
    });

    setDataUpdateBook({
      id: "",
      name: "",
      genre: "",
      authorId: "",
    });
    backButtonRef.current.click();
  };

  const handleDataChange = (event) => {
    setDataUpdateBook((prevBook) => ({
      ...prevBook,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <>
      {backButton && (
        <Button
          variant="outline"
          size="lg"
          className="p-0 mt-4"
          onClick={onUpdateData}
          ref={backButtonRef}
        >
          <FontAwesomeIcon icon={faArrowLeft} />
        </Button>
      )}
      <h1 className="text-center text-info">{title}</h1>

      <>
        {fields.length === 0 || dataTable.length === 0 ? (
          <Row>
            <Col md={12} className="text-center mt-4">
              <h2>---No Data---</h2>
            </Col>
          </Row>
        ) : (
          <>
            <Row>
              {fields.map((field, index) => (
                <Col
                  className="text-center border "
                  key={index}
                  md={
                    DEFAULT_WIDTH % fields.length !== 0 &&
                    index === fields.length - 1
                      ? fields.length - 1
                      : widthColumns
                  }
                >
                  {field}
                </Col>
              ))}
            </Row>
            <Row>
              {dataTable.map((data, index) => (
                <Col
                  className="text-center border"
                  key={index}
                  style={{
                    fontSize: "0.8rem",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                  md={
                    fields.length % 2 !== 0 &&
                    typeof data === "string" &&
                    data.split(" ")[0] === "Update"
                      ? fields.length - 1
                      : widthColumns
                  }
                >
                  {typeof data === "string" &&
                  data.split(" ")[0] === "Update" ? (
                    <Button
                      variant="outline-primary"
                      size="sm"
                      className="m-1"
                      onClick={() => onUpdateData(data.split(" ")[1])}
                    >
                      Update
                    </Button>
                  ) : updateFields && typeof data === "string" ? (
                    <input
                      type="text"
                      name={fields[index]}
                      value={Object.values(dataUpdateBook)[index]}
                      onChange={handleDataChange}
                      autoComplete="off"
                      className="text-center"
                      style={{ width: "100%" }}
                    />
                  ) : (
                    data
                  )}
                </Col>
              ))}
            </Row>
          </>
        )}
      </>

      {updateFields && (
        <Row>
          <Col className="text-center pt-4">
            <Button className="primary" onClick={() => handleUpdateInfoBook()}>
              Update Info Book
            </Button>
          </Col>
        </Row>
      )}
    </>
  );
}

export default Table;
