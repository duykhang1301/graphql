import { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { useMutation } from "@apollo/client";
import { getAuthors } from "../graphql-client/queries";
import { addSingleAuthor } from "../graphql-client/mutation";

function AuthorForm() {
  const [newAuthor, setNewAuthor] = useState({
    name: "",
    age: "",
  });
  const { name, age } = newAuthor;
  const onInputChange = (event) => {
    setNewAuthor((prevAuthor) => ({
      ...prevAuthor,
      [event.target.name]: event.target.value,
    }));
  };

  const onSubmit = (event) => {
    event.preventDefault();

    addAuthor({
      variables: {
        name,
        age: +age,
      },
      refetchQueries: [{ query: getAuthors }],
    });

    setNewAuthor({
      name: "",
      age: "",
    });
  };

  const [addAuthor, dataMutation] = useMutation(addSingleAuthor);
  return (
    <>
      <Form onSubmit={onSubmit}>
        <Form.Group className="invisible">
          <Form.Control
            type="text"
            placeholder="Genre"
            name="name"

            //onChange={onIuputChange}
            //value={name}
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            type="text"
            placeholder="Author name"
            name="name"
            onChange={onInputChange}
            value={name}
          />
        </Form.Group>
        <Form.Group>
          <Form.Control
            type="number"
            placeholder="Author age"
            name="age"
            onChange={onInputChange}
            value={age}
          />
        </Form.Group>
        <Button className="float-right" variant="info" type="submit">
          Add Author
        </Button>
      </Form>
    </>
  );
}

export default AuthorForm;
