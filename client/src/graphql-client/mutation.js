const { gql } = require("@apollo/client");

const addSingleBook = gql`
  mutation addSingleBookMutation($name: String, $genre: ID!, $authorId: ID!) {
    createBook(name: $name, genre: $genre, authorId: $authorId) {
      id
      name
    }
  }
`;

const addSingleAuthor = gql`
  mutation addSingleAuthorMutation($name: String, $age: Int) {
    createAuthor(name: $name, age: $age) {
      name
    }
  }
`;

const updateSingleBook = gql`
  mutation updateSingleBookMutation(
    $id: ID!
    $name: String
    $genre: ID!
    $authorId: ID!
  ) {
    updateBook(id: $id, name: $name, genre: $genre, authorId: $authorId) {
      name
      genre {
        name
      }
    }
  }
`;

const addGenre = gql`
  mutation addGenreMutation($name: String) {
    createGenre(name: $name) {
      name
    }
  }
`;

export { addSingleBook, addSingleAuthor, updateSingleBook, addGenre };
