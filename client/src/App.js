import Container from "react-bootstrap/Container";
import BookList from "./component/BookList";
import Forms from "./component/Forms";

import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import Button from "react-bootstrap/esm/Button";
import { useState } from "react";
import DetailsBookPage from "./component/DetailsBookPage";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
  cache: new InMemoryCache({
    addTypename: false,
  }),
});

function App() {
  const [detailPage, setDetailPage] = useState(false);
  return (
    <ApolloProvider client={client}>
      <Container className="pl-0 pt-2">
        <Button
          className="mr-2"
          variant={!detailPage ? "primary" : "outline-primary"}
          onClick={() => setDetailPage(false)}
        >
          View Book
        </Button>
        <Button
          variant={detailPage ? "primary" : "outline-primary"}
          onClick={() => setDetailPage(true)}
        >
          Book Management
        </Button>
      </Container>
      {!detailPage ? (
        <Container
          className="py-3 mt-3"
          style={{ backgroundColor: "lightcyan" }}
        >
          <h1 className="text-center text-info mb-3">My Books</h1>
          <hr />
          <Forms />
          <hr />
          <BookList />
        </Container>
      ) : (
        <Container className="mb-5">
          <DetailsBookPage />
        </Container>
      )}
    </ApolloProvider>
  );
}

export default App;
