const { gql } = require("apollo-server-express");

const typeDefs = gql`
  scalar Upload
  type Book {
    id: ID
    name: String
    genre: Genre
    author: Author
    thumbUrl: String
    pages: Int
    description: String
    createdAt: String
    updatedAt: String
  }

  type Author {
    id: ID
    name: String
    age: Int
    books: [Book]
  }

  type Genre {
    id: ID
    name: String
  }

  type PageInfo {
    totalPages: Int
  }

  #ROOT TYPE
  type Query {
    books(page: Int, limit: Int): [Book]
    book(id: ID!): Book
    authors: [Author]
    author(id: ID!): Author
    genres: [Genre]
    genre(id: ID!): Genre
    pageInfo(page: Int, limit: Int): PageInfo
  }

  type Mutation {
    createAuthor(name: String, age: Int): Author
    createBook(
      name: String
      genreId: ID!
      authorId: ID!
      thumbUrl: Upload!
      pages: Int
      description: String
    ): Book
    updateBook(id: ID!, name: String, genre: ID!, authorId: ID!): Book
    createGenre(name: String): Genre
  }
`;

module.exports = typeDefs;
