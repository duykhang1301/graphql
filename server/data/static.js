const books = [
  {
    id: 1,
    name: "Tat Den",
    genre: "Tieu thuyet",
    authorId: 1,
  },
  {
    id: 2,
    name: "Chi Pheo",
    genre: "Truyen Ngan",
    authorId: 2,
  },
  {
    id: 3,
    name: "Doi thua",
    genre: "Truyen Ngan",
    authorId: 2,
  },
  {
    id: 4,
    name: "Song mon",
    genre: "Tieu thuyet",
    authorId: 2,
  },
];

const authors = [
  {
    id: 1,
    name: "Ngo Tat To",
    age: 127,
  },
  {
    id: 2,
    name: "Nam Cao",
    age: 106,
  },
  {
    id: 3,
    name: "Vu Trong Phung",
    age: 109,
  },
];

module.exports = {
  books,
  authors,
};
