const { createWriteStream } = require("fs");
const path = require("path");
const appRoot = require("app-root-path");

const Book = require("../models/Book");
const Author = require("../models/Author");
const Genre = require("../models/Genre");

const mongoDataMethods = {
  getAllBooks: async (condition) => {
    if (Object.keys(condition).length === 0) {
      const result = await Book.find();
      return result;
    } else {
      const { page, limit } = condition;
      const skip = (page - 1) * limit;
      return await Book.find().skip(skip).limit(limit);
    }
  },
  getPageInfo: async ({ page, limit }) => {
    const count = await Book.countDocuments();
    const totalPages = Math.ceil(count / limit);
    return { totalPages };
  },
  getBookById: async (id) => await Book.findById(id),
  getAllAuthors: async () => await Author.find(),
  getAuthorById: async (id) => {
    return await Author.findById(id);
  },
  getAllGenres: async () => await Genre.find(),
  getGenreById: async (id) => {
    return await Genre.findById(id);
  },
  createAuthor: async (args) => {
    const newAuthor = new Author(args);
    return await newAuthor.save();
  },
  createBook: async (args) => {
    const { thumbUrl, ...others } = args;
    const { createReadStream, filename } = await thumbUrl;
    const customFileName = `${Date.now()}-${filename}`;
    const location = path.join(
      appRoot.path,
      `/public/images/${customFileName}`
    );
    const stream = createReadStream();

    await stream.pipe(createWriteStream(location));

    const URL_IMAGES = `http://localhost:4000/images/${customFileName}`;
    const newArgs = {
      ...others,
      thumbUrl: URL_IMAGES,
    };
    const newBook = new Book(newArgs);
    return await newBook.save();
  },
  updateBook: async (id, args) => {
    return await Book.findByIdAndUpdate(id, args, { new: true });
  },
  createGenre: async (args) => {
    const newGenre = new Genre(args);
    return await newGenre.save();
  },
};

module.exports = mongoDataMethods;
