const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CollectionSchema = new Schema({
  name: {
    type: String,
  },
  bookId: {
    type: String,
  },
  userId: {
    type: String,
  },
});

module.exports = mongoose.model("collections", CollectionSchema);
