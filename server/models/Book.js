const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const BookSchema = new Schema({
  name: {
    type: String,
  },
  genreId: {
    type: String,
  },
  authorId: {
    type: String,
  },
  thumbUrl: {
    type: String,
  },
  pages: {
    type: Number,
  },
  description: {
    type: String,
  },
  createdAt: {
    type: String,
    default: `${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}`,
  },
  updatedAt: {
    type: String,
    default: `${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}`,
  },
});

module.exports = mongoose.model("books", BookSchema);
