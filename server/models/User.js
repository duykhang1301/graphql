const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  email: {
    type: String,
  },
  googleId: {
    type: String,
  },
});

module.exports = mongoose.model("users", UserSchema);
