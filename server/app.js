const { graphqlUploadExpress } = require("graphql-upload");
const express = require("express");
const { ApolloServer } = require("apollo-server-express");

const mongoose = require("mongoose");

//Load schema and resolvers

const typeDefs = require("./schema/schema");

const resolvers = require("./resolver/resolver");
//connect DB

const connectDB = async () => {
  try {
    await mongoose.connect(
      "mongodb+srv://gopy_users:0903489605@cluster0.ifzzvlg.mongodb.net/?retryWrites=true&w=majority",
      // "mongodb://localhost:27017/GraphQL",
      {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      }
    );
    console.log("MongoDB connected");
  } catch (err) {
    console.log(err.message);
    process.exit(1);
  }
};
connectDB();

const mongoDataMethods = require("./data/db");

const server = new ApolloServer({
  uploads: false,
  typeDefs,
  resolvers,
  context: () => ({ mongoDataMethods }),
});

const app = express();

app.use(express.static("public"));
app.use(graphqlUploadExpress());
server.applyMiddleware({ app });

app.listen({ port: 4000 }, () => {
  console.log(`Server ready at http://localhost:4000${server.graphqlPath}`);
});
