const { GraphQLUpload } = require("graphql-upload");

const resolvers = {
  Query: {
    books: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllBooks(args),
    book: async (parent, { id }, { mongoDataMethods }) =>
      await mongoDataMethods.getBookById(id),
    authors: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllAuthors(),
    author: async (parent, { id }, { mongoDataMethods }) =>
      await mongoDataMethods.getAuthorById(id),
    genres: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllGenres(),
    genre: async (parent, { id }, { mongoDataMethods }) =>
      await mongoDataMethods.getGenreById(id),
    pageInfo: async (parent, args, { mongoDataMethods }) => {
      return await mongoDataMethods.getPageInfo(
        Object.keys(args).length === 0 ? { page: 1, limit: 10 } : args
      );
    },
  },
  Book: {
    author: async ({ authorId }, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAuthorById(authorId),
    genre: async ({ genreId }, args, { mongoDataMethods }) =>
      await mongoDataMethods.getGenreById(genreId),
    createdAt: (parent) => parent.createdAt,
    updatedAt: (parent) => parent.updatedAt,
  },
  Author: {
    books: async ({ id }, args, { mongoDataMethods }) =>
      await mongoDataMethods.getAllBooks({ authorId: id }),
  },

  Mutation: {
    createAuthor: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.createAuthor(args),
    createBook: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.createBook(args),
    updateBook: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.updateBook(args.id, args),
    createGenre: async (parent, args, { mongoDataMethods }) =>
      await mongoDataMethods.createGenre(args),
  },
  Upload: GraphQLUpload,
};

module.exports = resolvers;
